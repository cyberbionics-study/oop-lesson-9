import shelve
'''while using this module you should set quit and back commands constants in your UI code before calling functions'''


def write_dict_file(file_name):
    """Write dict in file"""
    print('Fill out the link shortener database.\n'
          f'to close enter {QUITCOMMAND}')
    with shelve.open(file_name) as link:
        while True:
            dict_key = input('Shortened link: ').strip().lower()
            if dict_key == f'{QUITCOMMAND}':
                print('-' * 50)
                break
            dict_value = input('Link: ').strip().lower()
            link[dict_key] = dict_value


def open_dict_file(file_name):
    """Open dict file and search"""
    print('', 'Searching links by shortened form.\n'
              f' to close enter {QUITCOMMAND}\n'
              f'to open adding menu again enter {BACKCOMMAND}')
    with shelve.open(file_name) as link:
        while True:
            short_link = input('Shortened link: ').strip().lower()
            if short_link == f'{QUITCOMMAND}':
                print('-' * 50)
                break
            if short_link == f'{BACKCOMMAND}':
                print('-' * 50)
                write_dict_file(file_name)
            elif short_link in link:
                print(f'{short_link}: {link[short_link]}')
            else:
                print(f'Shortened link: {short_link} does not exist.')


if __name__ == '__main__':
    QUITCOMMAND = 1
    BACKCOMMAND = 2
    name = "t2.txt"
    write_dict_file(name)
    open_dict_file(name)
