def sieve_of_eratosthenes(n):
    is_prime = [True] * (n + 1)
    is_prime[0] = is_prime[1] = False

    for current in range(2, int(n**0.5) + 1):
        if is_prime[current]:
            for multiple in range(current * current, n + 1, current):
                is_prime[multiple] = False

    prime_numbers = [i for i in range(2, n + 1) if is_prime[i]]

    return prime_numbers


if __name__ == '__main__':
    print(sieve_of_eratosthenes(100))
